# Thunderdome 2022 Livestream

Data files regarding livestream rips

## Description

The project contains 14 dirs with sfv, nfo & jpg files inside

## Contents

- [Thunderdome.2022.Angerfist.Tha.Playah.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Angerfist.Tha.Playah.1080p.WEB.x264)
- [Thunderdome.2022.Broken.Minds.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Broken.Minds.1080p.WEB.x264)
- [Thunderdome.2022.DRS.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.DRS.1080p.WEB.x264)
- [Thunderdome.2022.Deadly.Guns.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Deadly.Guns.1080p.WEB.x264)
- [Thunderdome.2022.Drokz.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Drokz.1080p.WEB.x264)
- [Thunderdome.2022.Mad.Dog.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Mad.Dog.1080p.WEB.x264)
- [Thunderdome.2022.Marc.Acardipane.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Marc.Acardipane.1080p.WEB.x264)
- [Thunderdome.2022.N-Vitral.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.N-Vitral.1080p.WEB.x264)
- [Thunderdome.2022.Nosferatu.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Nosferatu.1080p.WEB.x264)
- [Thunderdome.2022.Ophidian.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Ophidian.1080p.WEB.x264)
- [Thunderdome.2022.Spitnoise.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.Spitnoise.1080p.WEB.x264)
- [Thunderdome.2022.The.First.Decade.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.The.First.Decade.1080p.WEB.x264)
- [Thunderdome.2022.The.Prophets.Final.Exam.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.The.Prophets.Final.Exam.1080p.WEB.x264)
- [Thunderdome.2022.The.Second.Decade.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.The.Second.Decade.1080p.WEB.x264)
- [Thunderdome.2022.The.Third.Decade.1080p.WEB.x264](https://gitlab.com/1gabba/thunderdome-2022-livestream/-/tree/main/Thunderdome.2022.The.Third.Decade.1080p.WEB.x264)


## Video content

All video content can be downloaded from [1gabba.pw](https://1gabba.pw):
- https://1gabba.pw/node/63126/thunderdome-2022-angerfist-tha-playah-1080p-web-x264
- https://1gabba.pw/node/63127/thunderdome-2022-broken-minds-1080p-web-x264
- https://1gabba.pw/node/63128/thunderdome-2022-deadly-guns-1080p-web-x264
- https://1gabba.pw/node/63129/thunderdome-2022-drs-1080p-web-x264
- https://1gabba.pw/node/63588/thunderdome-2022-drokz-1080p-web-x264
- https://1gabba.pw/node/63144/thunderdome-2022-mad-dog-1080p-web-x264
- https://1gabba.pw/node/63145/thunderdome-2022-marc-acardipane-1080p-web-x264
- https://1gabba.pw/node/63146/thunderdome-2022-nosferatu-1080p-web-x264
- https://1gabba.pw/node/63147/thunderdome-2022-n-vitral-1080p-web-x264
- https://1gabba.pw/node/63158/thunderdome-2022-ophidian-1080p-web-x264
- https://1gabba.pw/node/63159/thunderdome-2022-spitnoise-1080p-web-x264
- https://1gabba.pw/node/63160/thunderdome-2022-the-prophets-final-exam-1080p-web-x264
- https://1gabba.pw/node/63171/thunderdome-2022-the-second-decade-1080p-web-x264
- https://1gabba.pw/node/63172/thunderdome-2022-the-third-decade-1080p-web-x264
- https://1gabba.pw/node/63170/thunderdome-2022-the-first-decade-1080p-web-x264

Enjoy

## Audio content

Audio stream rip can be downloaded from 1gabba.pw:

`Various_Artists-Thunderdome_Never_Dies_Livestream-WEB-12-12-2022-CiN_INT`

https://1gabba.pw/node/62703/various-artists-thunderdome-never-dies-livestream-web-12-12-2022-cin-int

Thunderdome livesets can be downloaded here:

`Thunderdome_2022-Livesets-WEB-12-10-2022-CiN_INT`

https://1gabba.pw/node/62745/thunderdome-2022-livesets-web-12-10-2022-cin-int

***
